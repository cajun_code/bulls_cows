Bulls and Cows
===============

Bulls and Cows is an Isogram game where the computer thinks of a word and you try to guess it.

Installation 
-----------------

Windows
^^^^^^^^^^^^

1. Download Cmake from cmake.org and Install it.
2. Make a new directory called "build":: 
  
  mkdir build
  cd build
  
3. Run::

  cmake -G "Visual Studio 14 2015" ..

4. Open Visual Studio 2015 Community Edition
5. Click File-> Open->Project/Solution  and find the sln file in the build directory, Click Open
6. In the Solution, Right click on the libbulls_cows project and select properties.
7. Make sure the Configuration type is set to "Static Library" and click OK.
8. Right click on the bulls_cows project and click on "Debug"


Mac/Linux
^^^^^^^^^^^^

1. Download Cmake from cmake.org and Install it.
    
    * On a mac you can just use homebew::
        
        brew install cmake
    
    * Linux can install it from the package manager such as apt-get

2. Make a new directory called "build":: 
  
  mkdir build
  cd build
  
3. Run::

  cmake -G "Unix Makefiles" ..  
  
 4. Run Make
 5. Run Make Test 
 6. Run the game "./main/bulls_cows
 
 