/* 
 * File:   FBullCow.cpp
 * Author: alley
 * 
 * Created on February 22, 2016, 10:03 PM
 */

#include "FBullCow.h"

FBullCow::FBullCow() {
    this->Reset();
}

FBullCow::FBullCow(const FBullCow& orig) {
}

FBullCow::~FBullCow() {
}

void FBullCow::Reset() {
     maxTries = 8;
    currentTry = 1;
    hiddenWord = "planet";
    gameWon = false;
}

int32 FBullCow::GetCurrentTry() const {return currentTry;}
int32 FBullCow::GetMaxTires() const {return maxTries;}
int32 FBullCow::GetHiddenWordLength() const {return hiddenWord.length();}
bool FBullCow::IsGameWon() const {return gameWon;}

EGuessStatus FBullCow::IsGuessValid(FString guess) const {
    if(!IsIsogram(guess)) 
        return EGuessStatus::NOT_ISOGRAM;
    if(!IsLowerCase(guess)) 
        return EGuessStatus::INCORRECT_CASE;
    if(guess.length() != GetHiddenWordLength())
        return EGuessStatus::INCORRECT_LETTER_COUNT;
    return EGuessStatus::OK;
}

FBullCowCount FBullCow::ProcessGuess(FString guess) {
    // increment the try counter
    currentTry++;
    FBullCowCount bcCount;
    for(int32 i = 0; i < hiddenWord.length(); i++){
        for(int32 j = 0; j < hiddenWord.length(); j++){
            if(hiddenWord[i] == guess[j]){
                if(i == j){
                    bcCount.bulls++;
                }else{
                    bcCount.cows++;
                }
            }
        }
    }
    if(bcCount.bulls == GetHiddenWordLength()){
        gameWon = true;
    }
    return bcCount;
}

bool FBullCow::IsIsogram(FString guess) const{
    TMap<char, bool> map;
    for(auto letter : guess){
        letter = tolower(letter);
        if(map.count(letter) != 0){
            return false;
        }else{
            map[letter] = true;
        }
    }
    return true;
}

bool FBullCow::IsLowerCase(FString guess) const {
    for(auto letter : guess){
        if(isupper(letter)){
            return false;
        }
    }
    return true;
    
}

