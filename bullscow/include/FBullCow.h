/* 
 * File:   FBullCow.h
 * Author: alley
 * 
 * Created on February 22, 2016, 10:03 PM
 */
#pragma once

#include <string>
#include <map>
// Mapping Standard types to Unreal 
#define TMap std::map
using FString = std::string;
using int32 = int;
/** Struct to map the returning calculation of processing a guess*/
struct FBullCowCount{
    int32 bulls = 0;
    int32 cows = 0;
};
/** Enum of values to the validity of a guess*/
enum class EGuessStatus{
    OK,
    NOT_ISOGRAM,
    INCORRECT_LETTER_COUNT,
    INCORRECT_CASE
};

/**
 * FBullCow is a to rep
 */
class FBullCow {
public:
    FBullCow();
    FBullCow(const FBullCow& orig);
    virtual ~FBullCow();
    
    void Reset(); 
    FBullCowCount ProcessGuess(FString guess);
    
    EGuessStatus IsGuessValid(FString guess) const;
    int32 GetMaxTires() const;
    int32 GetHiddenWordLength() const;
    int32 GetCurrentTry() const;
    bool IsGameWon() const;
    
private:
    int32 currentTry;
    int32 maxTries;
    FString hiddenWord;
    bool gameWon;
    
    bool IsIsogram(FString guess)const;
    bool IsLowerCase(FString guess)const;
};
