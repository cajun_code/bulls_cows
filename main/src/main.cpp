//
//  main.cpp
//  bull_cows
//
//  Created by Allan Davis on 1/15/16.
//  Copyright © 2016 Allan Davis. All rights reserved.
//

#include <iostream>
#include <string>
#include <FBullCow.h>

using FText = std::string;
using int32 = int;

void PrintIntro();
void PlayGame();
FText getGuess();
bool AskToPlayAgain();
void PrintGameSummary();

//constexpr int32 WORD_LENGTH = 5;
FBullCow BCGame;

/** Main start of application */
int main(int argc, const char * argv[]) {
    do {
        BCGame.Reset();
        PrintIntro();
        PlayGame();
    }while(AskToPlayAgain());
    std::cout << std::endl;
    return 0;
}

/** introduce the game*/
void PrintIntro(){
    std::cout << "Welcome to Bulls and Cows, A fun word game." << std::endl;
    std::cout << "Can you guess the " << BCGame.GetHiddenWordLength() <<" letter isogram I am thinking of?\n";
    std::cout << std::endl;
    return;
}

/** Play the Game */
void PlayGame(){
    
    FText guess = "";
     
    while(!BCGame.IsGameWon() && 
          BCGame.GetCurrentTry() <= BCGame.GetMaxTires() ) {
        guess = getGuess(); //todo validate guess
        
        FBullCowCount bcCount = BCGame.ProcessGuess(guess);
        std::cout << "Bulls = " << bcCount.bulls;
        std::cout << ", Cows = " << bcCount.cows << std::endl;
    }
    PrintGameSummary();
}

// get the guess from the player
FText getGuess(){
    FText temp = "";
    EGuessStatus status;
    do{
    std::cout <<"Try " << BCGame.GetCurrentTry() << " Please enter a guess: ";
    getline(std::cin, temp);
    status = BCGame.IsGuessValid(temp);
    switch(status){
        case EGuessStatus::INCORRECT_LETTER_COUNT:
            std::cout << "Please enter a word with " << BCGame.GetHiddenWordLength() << " letters.\n";
            break;
        case EGuessStatus::INCORRECT_CASE:
            std::cout << "Please enter a word in all lowercase. \n";
            break;
        case EGuessStatus::NOT_ISOGRAM:
            std::cout << "Please enter a word without repeating letters.\n ";
            break;
        default:
            break;
    }
    }while(status != EGuessStatus::OK);
    return temp;
    
}

void PrintGameSummary(){
    if(BCGame.IsGameWon()){
        std::cout << "WoW Great Job, You won.\n";
    }else{
        std::cout << "Bummer Dude, Better Luck again next time.\n";
    }
}


bool AskToPlayAgain() {
    FText response = "";
    std::cout << "Do you want to Play Again(y/n)? ";
    getline(std::cin, response);
    //transform(response.begin(), response.end(), response.begin(), ::tolower);
    return (tolower(response[0]) == 'y');
}
