#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <FBullCow.h>

FBullCow bcGame;
TEST_CASE("Reset sets max tries to zero", "[BullCows]"){
    bcGame.Reset();
    REQUIRE(bcGame.GetCurrentTry() == 1);
}

TEST_CASE("Testing guess", "[BullsCows]"){
    SECTION("Wrong Length input"){
        EGuessStatus status = bcGame.IsGuessValid("hi");
        REQUIRE(status == EGuessStatus::INCORRECT_LETTER_COUNT);
    }
    SECTION("Not an Isogram"){
        EGuessStatus status = bcGame.IsGuessValid("hello");
        REQUIRE(status == EGuessStatus::NOT_ISOGRAM);
    }
    SECTION("Incorrect LetterCassing"){
        EGuessStatus status = bcGame.IsGuessValid("Planet");
        REQUIRE(status == EGuessStatus::INCORRECT_CASE);
    }
    SECTION("Incorrect LetterCassing Mid Word"){
        EGuessStatus status = bcGame.IsGuessValid("plaNet");
        REQUIRE(status == EGuessStatus::INCORRECT_CASE);
    }
    SECTION("Good Guess"){
        EGuessStatus status = bcGame.IsGuessValid("planet");
        REQUIRE(status == EGuessStatus::OK);
    }
}

TEST_CASE("Processing Guess", "[BullsCows]"){
    SECTION("Process a wrong guess"){
        FBullCowCount count = bcGame.ProcessGuess("Plante");
        REQUIRE(count.bulls == 3);
        REQUIRE(count.cows == 2);
        REQUIRE(bcGame.GetCurrentTry() == 2);
    }
}
